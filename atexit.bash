LOGLEVELATEXIT=${LOGLEVELATEXIT:-INFO}

declare -A _LOGLEVELSATEXIT=(DEBUG 10 INFO 20 WARN 30 ERROR 40)

atexit() {
    local var= meta=
    while [ $# -gt 0 ]; do
        case "$1" in
            -v) declare -n var="$2"; shift 2 || return 1;;
            -m) meta="$2"; shift 2 || return 1;;
            -*) echo "[ERROR] [$BASHPID] atexit: invalid arguments: (${*@Q})" >&2; return 1;;
             *) break;;
        esac
    done
    declare -n index="_atexit_${BASHPID}"
    index=$((index+1))
    var="$BASHPID $index"
    declare -n cmd="_atexit_${BASHPID}_${index}"
    declare -n _meta="_atexit_${BASHPID}_${index}_meta"
    declare -n stat="_atexit_${BASHPID}_${index}_stat"
    cmd=("$@")
    _meta=$meta
    stat=PEND
    trap _atexit_ EXIT
}
_logatexit() {
    local level="$1" prefix="$2" action="$3" pid="$4" index="$5"; shift 5 || return 1
    declare -i ilevel=${_LOGLEVELSATEXIT[$level]}
    declare -i minlevel=${_LOGLEVELSATEXIT[$LOGLEVELATEXIT]}
    [ $ilevel -ge $minlevel ] || return
    declare -n cmd="_atexit_${pid}_${index}"
    declare -n meta="_atexit_${pid}_${index}_meta"
    declare -n stat="_atexit_${pid}_${index}_stat"
    printf "%-7s %8s %9s %8s %3s %6s: %s: %s (%s)\\n" \
        "[${level}]" "[${BASHPID}]" "${prefix}" "[${pid}]" "#${index}" "(${stat})" \
        "${meta@Q}" "${action}" "${cmd[*]@Q}" \
        >&2
}
_checkatexit() {
    local other=
    while [ $# -gt 0 ]; do
        case "$1" in
            -??*) set -- "${1:0:2}" -"${1:2}" "${@:2}";;
            -p) prefix="$2"; shift 2 || return 1;;
            -o) other=1; shift;;  # enable other pid's tasks
            -q) quiet=$((quiet+1)); shift;;
            -*) echo "[ERROR] [$BASHPID] $p: invalid arguments: (${@@Q})" >&2; return 1;;
             *) break;;
        esac
    done
    if [ $# -ne 2 ]; then
        echo "[ERROR] [$BASHPID] ${prefix}: invalid arguments: (${@@Q})" >&2
        return 1
    fi
    pid=$1
    index=$2
    if [ "$pid" != $BASHPID ]; then
        if [ $other ]; then
            [ $((quiet+0)) -ge 2 ] || _logatexit DEBUG "$prefix" "other pid's task" "$pid" "$index"
        else
            [ $((quiet+0)) -ge 2 ] || _logatexit ERROR "$prefix" "other pid's task" "$pid" "$index"
            return 1
        fi
    fi
    # echo "::: prefix:${prefix@Q}, other:${other@Q}, quiet: ${quiet@Q}, pid:${pid@Q}, index:${index@Q}" >&2
    return 0
}
notatexit() {
    local prefix=notatexit quiet= pid= index=
    _checkatexit "$@" || return 1
    declare -n stat="_atexit_${pid}_${index}_stat"
    if ! [ "$stat" = PEND ]; then
        [ $quiet ] || _logatexit WARN "$prefix" 'skipping non-pending task' "$pid" "$index"
    else
        [ $quiet ] || _logatexit DEBUG "$prefix" 'SKIP' "$pid" "$index"
        stat=SKIP
    fi
    return 0
}
nowatexit() {
    local prefix=nowatexit force= quiet= pid= index=
    _checkatexit "$@" || return 1
    declare -n stat="_atexit_${pid}_${index}_stat"
    if ! [ "$stat" = PEND ]; then
        [ "$prefix" = nowatexit ] && local level='WARN' || local level='DEBUG'
        [ $quiet ] || _logatexit "$level" "$prefix" 'skipping non-pending task' "$pid" "$index"
        return 0
    fi
    declare -n cmd="_atexit_${pid}_${index}"
    if [ ${#cmd[@]} -eq 0 ]; then
        [ $quiet ] || _logatexit INFO "$prefix" 'reporting DONE' "$pid" "$index"
        stat=DONE
        return 0
    fi
    "${cmd[@]}" || {
        local status=$?
        [ $((quiet+0)) -ge 3 ] || _logatexit ERROR "$prefix" "ERRR: $status" "$pid" "$index"
        stat=ERRR
        return $status
    } || return $?
    [ $quiet ] || _logatexit DEBUG "$prefix" 'DONE' "$pid" "$index"
    stat=DONE
    return 0
}
_atexit_() {
    local status=$?
    local _status=
    declare -n index="_atexit_${BASHPID}"
    index=$((index+0))
    while [ $index -gt 0 ]; do
        nowatexit -p _atexit_ $BASHPID $index || _status=1
        index=$((index-1))
    done
    [ $_status ] || exit $status
    exit 1
}
